//
//  OpenCVWrapper.m
//  ARFACE
//
//  Created by vrlab on 2017/12/23.
//  Copyright © 2017年 vrlab. All rights reserved.
//

#import "OpenCVWrapper.h"
#ifdef __cplusplus
#undef NO
#undef YES
#import <opencv2/opencv.hpp>
#import <opencv2/xfeatures2d.hpp>
#import <vector>
#endif
#import <UIKit/UIKit.h>
using namespace cv;
using namespace std;
cv::Point3f point2dTo3d(cv::Point3f& point);
@implementation OpenCVWrapper
- (UIImage *) isThisWorking:(UIImage *) input {
    Mat IMG = [OpenCVWrapper cvMatWithImage:input];
    Ptr<xfeatures2d::SIFT> sift = xfeatures2d::SIFT::create();
    UMat descriptors = UMat();
    std::vector<KeyPoint> keypoints = std::vector<KeyPoint>();
    sift->detectAndCompute(IMG, noArray(), keypoints, descriptors);
    
    //cout << IMG.type ;
    cvtColor(IMG, IMG, COLOR_RGBA2RGB);
    drawKeypoints(IMG, keypoints, IMG);
    //cvtColor(IMG, IMG, COLOR_RGB2RGBA);
    return [OpenCVWrapper UIImageFromCVMat:IMG];
}

+(void) testing:(const float [])array{
    
    cout << array[2] << endl;
}

+(double *) computeTransForm: (const UIImage*) IMG1 : (const UIImage*) IMG2 : (const float[])depthData :(int) w : (const float [])cameraIn:(float) low:(float) high:(int)iterCount:(float)error:(int)scaler {
    Mat img1 = [OpenCVWrapper cvMatWithImage:IMG1];
    Mat img2 = [OpenCVWrapper cvMatWithImage:IMG2];
    
    Ptr<xfeatures2d::SIFT> sift = xfeatures2d::SIFT::create();
    
    UMat descriptors1 = UMat();
    std::vector<KeyPoint> keypoints1 = std::vector<KeyPoint>();
    UMat descriptors2 = UMat();
    std::vector<KeyPoint> keypoints2 = std::vector<KeyPoint>();
    
    sift->detectAndCompute(img1, noArray(), keypoints1, descriptors1);
    sift->detectAndCompute(img2, noArray(), keypoints2, descriptors2);
    
    FlannBasedMatcher matcher = FlannBasedMatcher();
    vector<DMatch> matches = vector<DMatch>();
    matcher.match(descriptors1, descriptors2, matches);
    
    //vector<DMatch> goodMatches = vector<DMatch>();
    float maxDis = 0;
    for (int i = 0;i < matches.size();i++){
        if (matches[i].distance > maxDis){
            maxDis = matches[i].distance;
        }
    }
    
    int goodMatchsizes = 0;
    for (int i = 0;i < matches.size();i++){
        Point2f p = keypoints1[matches[i].queryIdx].pt;
        int count = (p.y) * w + (p.x);
        float d = depthData[count];
        if (d > low && d < high && matches[i].distance < maxDis / scaler){
            goodMatchsizes++;
        }
    }
        
    
    vector<Point2f> pts_img = vector<Point2f>(goodMatchsizes);
    vector<Point3f> pts_obj = vector<Point3f>(goodMatchsizes);
    int numbers = 0;
    for (int i = 0;i < matches.size(); i++){
        Point2f p = keypoints1[matches[i].queryIdx].pt;
        int count = (p.y) * w + (p.x);
        float d = depthData[count];
        if (d > low && d < high && matches[i].distance < maxDis / scaler){
            pts_img[numbers] = keypoints2[matches[i].trainIdx].pt;
            Point3f tmpPt; tmpPt.x = p.x; tmpPt.y = p.y; tmpPt.z = d;
            Point3f pd = point2dTo3d(tmpPt);
            pts_obj[numbers] = pd;
            numbers++;
        }
    }
    cout << "NUMBERS " << numbers << endl;
    Mat cameraMAT = (Mat_<float>(3,3) << cameraIn[0],0,cameraIn[6],0,cameraIn[4],cameraIn[7],0,0,1.0);
    cout << "CAMERA INS "<<cameraMAT << endl;
    Mat rvec; Mat tvec;

    
    solvePnPRansac(pts_obj, pts_img, cameraMAT, noArray(), rvec, tvec,false,iterCount,error);
    

    Mat_<double> lastRow = (Mat_<double>(1,4) << 0,0,0,1);
    Mat output;
    Rodrigues(rvec, output);
    hconcat(output, tvec, output);
    output.push_back(lastRow);
    cout << "TRANSFORM "<<output << endl;
    
    
    pts_img.shrink_to_fit();
    pts_obj.shrink_to_fit();
    matches.shrink_to_fit();
    keypoints1.shrink_to_fit();
    keypoints2.shrink_to_fit();
    cameraMAT.release();
    lastRow.release(); rvec.release(); tvec.release();
    descriptors1.release();descriptors2.release(); img1.release();img2.release();
    
    double * numberArray = new double[16];
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            int count = i * 4 + j;
            numberArray[count] = output.at<double>(i, j);
            //numberArray[count] = 1.0;
        }
    }
    
    return numberArray;

}


+ (cv::Mat) imRead:(const char *) fileName {
    return cv::imread(fileName);
}

//+(UIImage *)flipnrot:(UIImage *)image{
//    cv::Mat tmp = [OpenCVWrapper cvMatWithImage:image];
//    cv::Mat tmp2;
//    transpose(tmp, tmp);
//    return [OpenCVWrapper UIImageFromCVMat:tmp];
//}
//+(UIImage *)flipsav:(UIImage *)image{
//    cv::Mat tmp = [OpenCVWrapper cvMatWithImage:image];
//    cv::Mat tmp2;
//    transpose(tmp, tmp);
//    //rotate(tmp, tmp2, 0);
//    tmp.convertTo(tmp2, CV_RGBA2RGB);
//    return [OpenCVWrapper UIImageFromCVMat:tmp2];
//}

+(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat {
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    
    CGColorSpaceRef colorSpace;
    CGBitmapInfo bitmapInfo;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
        bitmapInfo = kCGImageAlphaNone | kCGBitmapByteOrderDefault;
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
        bitmapInfo = kCGBitmapByteOrder32Little | (
                                                   cvMat.elemSize() == 3? kCGImageAlphaNone : kCGImageAlphaNoneSkipFirst
                                                   );
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(
                                        cvMat.cols,                 //width
                                        cvMat.rows,                 //height
                                        8,                          //bits per component
                                        8 * cvMat.elemSize(),       //bits per pixel
                                        cvMat.step[0],              //bytesPerRow
                                        colorSpace,                 //colorspace
                                        bitmapInfo,                 // bitmap info
                                        provider,                   //CGDataProviderRef
                                        NULL,                       //decode
                                        false,                      //should interpolate
                                        kCGRenderingIntentDefault   //intent
                                        );
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}

+ (cv::Mat)cvMatWithImage:(const UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    size_t numberOfComponents = CGColorSpaceGetNumberOfComponents(colorSpace);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels
    CGBitmapInfo bitmapInfo = kCGImageAlphaNoneSkipLast | kCGBitmapByteOrderDefault;
    
    // check whether the UIImage is greyscale already
    if (numberOfComponents == 1){
        cvMat = cv::Mat(rows, cols, CV_8UC1); // 8 bits per component, 1 channels
        bitmapInfo = kCGImageAlphaNone | kCGBitmapByteOrderDefault;
    }
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,             // Pointer to backing data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    bitmapInfo);              // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

@end

cv::Point3f point2dTo3d(cv::Point3f& point)
{
    cv::Point3f p; // 3D 点
    p.z = double( point.z );
    p.x = ( point.x - 180.0) * p.z / 500.872600349;
    p.y = ( point.y - 320.0) * p.z / 500.872600349;
    return p;
}
