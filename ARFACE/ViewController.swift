//
//  ViewController.swift
//  ARFACE
//
//  Created by vrlab on 2017/12/2.
//  Copyright © 2017年 vrlab. All rights reserved.
//
// API 中有任何不理解的地方的时候，按住键盘 Option 键然后用鼠标单击不理解的部分。

import UIKit
import SceneKit
import ARKit
import MessageUI
import Surge

var btnTouched = false



extension String {
    func appendLineToURL(fileURL: URL) throws {
        try (self + "\n").appendToURL(fileURL: fileURL)
    }
    
    func appendToURL(fileURL: URL) throws {
        let data = self.data(using: String.Encoding.utf8)!
        try data.append(fileURL: fileURL)
    }
}



extension Data {
    func append(fileURL: URL) throws {
        if let fileHandle = FileHandle(forWritingAtPath: fileURL.path) {
            defer {
                fileHandle.closeFile()
            }
            fileHandle.seekToEndOfFile()
            fileHandle.write(self)
        }
        else {
            try write(to: fileURL, options: .atomic)
        }
    }
}

func writeTxtFile(fileName:String,textToWrite:String)->URL?{
    if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
        let fileURL = dir.appendingPathComponent(fileName)
        //writing
        do {
            try textToWrite.write(to: fileURL, atomically: false, encoding: .utf8)
        }
        catch {/* error handling here */}
        //reading
        do {
            let _ = try String(contentsOf: fileURL, encoding: .utf8)
        }
        catch {/* error handling here */}
        return fileURL
    }
    return nil
}

struct Pixel {
    var R:UInt8
    var G:UInt8
    var B:UInt8
}

func writeDepthCSV(depthData:[Float],width:Int,height:Int,filename:String)->URL{
    let dir: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last! as URL
    let url = dir.appendingPathComponent(filename)
    writeTxtFile(fileName: filename, textToWrite: "")
    do{
        let fileHandle = try FileHandle(forWritingTo: url)
        for j in 0..<height{
            for i in 0..<width{
                let count = j * width + i
                let tmpData = "\(depthData[count]),".data(using: String.Encoding.utf8, allowLossyConversion: false)!
                fileHandle.seekToEndOfFile()
                fileHandle.write(tmpData)
            }
            let tmpData = "\n".data(using: String.Encoding.utf8, allowLossyConversion: false)!
            fileHandle.seekToEndOfFile()
            fileHandle.write(tmpData)
        }
        fileHandle.closeFile()
    }catch{
        
    }
    return url
}

class PointCloud{
    var cloudXYZ:Matrix<Float>
    var cloudRGB:[Pixel]
    var pointCount:Int
    init (cloudData:[[Float]],colorData:[Pixel]){
        cloudXYZ = transpose(Matrix<Float>(cloudData))
        cloudRGB = colorData
        pointCount = cloudXYZ.columns
        
    }
    init (cloudDataMatrix:Matrix<Float>,colorData:[Pixel]){
        cloudXYZ = cloudDataMatrix
        cloudRGB = colorData
        pointCount = cloudXYZ.columns
    }
    
    
    func writePlyToDoc(filename:String)->URL?{
        var tmpText = "ply\nformat ascii 1.0\ncomment created by App\nelement vertex \(pointCount)\nproperty float32 x\nproperty float32 y\nproperty float32 z\nproperty uchar red\nproperty uchar green\nproperty uchar blue\nend_header\n"

        for col in 0..<cloudXYZ.columns{
            tmpText += "\(cloudXYZ[0,col]) \(cloudXYZ[1,col]) \(cloudXYZ[2,col]) \(cloudRGB[col].R) \(cloudRGB[col].G) \(cloudRGB[col].B)\n"
        }
        writeTxtFile(fileName: filename, textToWrite: tmpText)
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else{
            return nil
        }
        let fileURL = dir.appendingPathComponent(filename)
        return fileURL
    }
    
    func writePlyToDoc(filename:String,min:Float,max:Float)->URL?{
       
        var count = 0
        for col in 0..<cloudXYZ.columns{
            if cloudXYZ[2,col] > min && cloudXYZ[2,col] < max {
                count += 1
                //tmpText += "\(cloudXYZ[0,col]) \(cloudXYZ[1,col]) \(cloudXYZ[2,col]) \(cloudRGB[col].R) \(cloudRGB[col].G) \(cloudRGB[col].B)\n"
            }
        }
        let plytext = "ply\nformat ascii 1.0\ncomment created by App\nelement vertex \(count)\nproperty float32 x\nproperty float32 y\nproperty float32 z\nproperty uchar red\nproperty uchar green\nproperty uchar blue\nend_header\n"
        
        do{
            let dir: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last! as URL
            let url = dir.appendingPathComponent(filename)
            writeTxtFile(fileName: filename, textToWrite: "")
            let fileHandle = try FileHandle(forWritingTo: url)
            let data = plytext.data(using: String.Encoding.utf8, allowLossyConversion: false)!
            fileHandle.seekToEndOfFile()
            fileHandle.write(data)

            for col in 0..<cloudXYZ.columns{
                DispatchQueue.main.async {
                    
                }
                if cloudXYZ[2,col] > min && cloudXYZ[2,col] < max {
                    
                    let tmpData = "\(cloudXYZ[0,col]) \(cloudXYZ[1,col]) \(cloudXYZ[2,col]) \(cloudRGB[col].R) \(cloudRGB[col].G) \(cloudRGB[col].B)\n".data(using: String.Encoding.utf8, allowLossyConversion: false)!
                    fileHandle.seekToEndOfFile()
                    fileHandle.write(tmpData)
                }
            }
            fileHandle.closeFile()
        }catch{
            
        }
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else{
            return nil
        }
        let fileURL = dir.appendingPathComponent(filename)
        return fileURL
    }
    
    func transform (transforMAT:UnsafeMutablePointer<Double>){
        var T = Matrix<Float>(rows: 4, columns: 4, repeatedValue: 0)
        for col in 0..<4 {
            for row in 0..<4 {
                let count = col * 4 + row
                T[row,col] = Float(transforMAT[count])
            }
        }
        cloudXYZ = T * cloudXYZ
    }
    func transform (transforMAT:[Double]){
        var T = Matrix<Float>(rows: 4, columns: 4, repeatedValue: 0)
        for col in 0..<4 {
            for row in 0..<4 {
                let count = col * 4 + row
                T[row,col] = Float(transforMAT[count])
            }
        }
        cloudXYZ = T * cloudXYZ
    }
    
    static func transform(origin:PointCloud,transforMAT:UnsafeMutablePointer<Double>) -> PointCloud {
        let outCloud = PointCloud(cloudDataMatrix: origin.cloudXYZ, colorData: origin.cloudRGB)
        outCloud.transform(transforMAT: transforMAT)
        return outCloud
    }
    
    static func + (lhs: PointCloud, rhs: PointCloud) -> PointCloud{
        var tmpPointCloudData = Matrix<Float>(rows: 4, columns: lhs.cloudXYZ.columns+rhs.cloudXYZ.columns, repeatedValue: 0)
        let tmpColorData = lhs.cloudRGB + rhs.cloudRGB
        for i in 0..<lhs.cloudXYZ.columns{
            tmpPointCloudData[0,i] = lhs.cloudXYZ[0,i]
            tmpPointCloudData[1,i] = lhs.cloudXYZ[1,i]
            tmpPointCloudData[2,i] = lhs.cloudXYZ[2,i]
            tmpPointCloudData[3,i] = lhs.cloudXYZ[3,i]
        }
        for i in 0..<rhs.cloudXYZ.columns{
            tmpPointCloudData[0,i+lhs.cloudXYZ.columns] = rhs.cloudXYZ[0,i]
            tmpPointCloudData[1,i+lhs.cloudXYZ.columns] = rhs.cloudXYZ[1,i]
            tmpPointCloudData[2,i+lhs.cloudXYZ.columns] = rhs.cloudXYZ[2,i]
            tmpPointCloudData[3,i+lhs.cloudXYZ.columns] = rhs.cloudXYZ[3,i]
        }
        return PointCloud(cloudDataMatrix: tmpPointCloudData, colorData: tmpColorData)
    }
    
    
}

func saveImageToDocumentDirectory(_ chosenImage: UIImage,name:String) -> URL {
    let directoryPath =  NSHomeDirectory().appending("/Documents/")
    if !FileManager.default.fileExists(atPath: directoryPath) {
        do {
            try FileManager.default.createDirectory(at: NSURL.fileURL(withPath: directoryPath), withIntermediateDirectories: true, attributes: nil)
        } catch {
            print(error)
        }
    }
    
    let filename = name.appending(".png")
    let filepath = directoryPath.appending(filename)
    let url = NSURL.fileURL(withPath: filepath)
    do {
        try UIImagePNGRepresentation(chosenImage)?.write(to: url, options: .atomic)
       
        
    } catch {
        print(error)
        print("file cant not be save at path \(filepath), with error : \(error)");
        
    }
    return url
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension CVPixelBuffer{
    var image:UIImage {
        let ciImage = CIImage(cvPixelBuffer: self)
        let temporaryContext = CIContext(options: nil)
        let videoImage = temporaryContext.createCGImage(ciImage, from: CGRect(x: 0, y: 0, width: CVPixelBufferGetWidth(self), height: CVPixelBufferGetHeight(self)))
        let image = UIImage(cgImage: videoImage!)
        return image
    }
    
}
extension UIImage {

    
    func resizeImage(size:CGSize)->UIImage{
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        self.draw(in:rect)
        return UIGraphicsGetImageFromCurrentImageContext()!
    }
    
    var colorData: [UInt8] {
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        
        guard let cgImage = cgImage else {
            return []
        }
        let width = Int(size.width)
        let height = Int(size.height)
        
        var rawData = [UInt8](repeating: 0, count: width * height * 4)
        let bytesPerPixel = 4
        let bytesPerRow = bytesPerPixel * width
        let bytesPerComponent = 8
        
        let bitmapInfo = CGImageAlphaInfo.premultipliedLast.rawValue | CGBitmapInfo.byteOrder32Big.rawValue
        
        let context = CGContext(data: &rawData,
                                width: width,
                                height: height,
                                bitsPerComponent: bytesPerComponent,
                                bytesPerRow: bytesPerRow,
                                space: colorSpace,
                                bitmapInfo: bitmapInfo)
        
        let drawingRect = CGRect(origin: .zero, size: CGSize(width: width, height: height))
        context?.draw(cgImage, in: drawingRect)
        return rawData
    }
    struct RotationOptions: OptionSet {
        let rawValue: Int
        
        static let flipOnVerticalAxis = RotationOptions(rawValue: 1)
        static let flipOnHorizontalAxis = RotationOptions(rawValue: 2)
    }
    
    func rotated(by rotationAngle: Measurement<UnitAngle>, options: RotationOptions = []) -> UIImage? {
        guard let cgImage = self.cgImage else { return nil }
        
        let rotationInRadians = CGFloat(rotationAngle.converted(to: .radians).value)
        let transform = CGAffineTransform(rotationAngle: rotationInRadians)
        var rect = CGRect(origin: .zero, size: self.size).applying(transform)
        rect.origin = .zero
        
        let renderer = UIGraphicsImageRenderer(size: rect.size)
        return renderer.image { renderContext in
            renderContext.cgContext.translateBy(x: rect.midX, y: rect.midY)
            renderContext.cgContext.rotate(by: rotationInRadians)
            
            let x = options.contains(.flipOnVerticalAxis) ? -1.0 : 1.0
            let y = options.contains(.flipOnHorizontalAxis) ? 1.0 : -1.0
            renderContext.cgContext.scaleBy(x: CGFloat(x), y: CGFloat(y))
            
            let drawRect = CGRect(origin: CGPoint(x: -self.size.width/2, y: -self.size.height/2), size: self.size)
            renderContext.cgContext.draw(cgImage, in: drawRect)
        }
    }
}

class ViewController: UIViewController,ARSCNViewDelegate,ARSessionDelegate,MFMailComposeViewControllerDelegate {
    var imgCount:Int = 0;
    var captured = false;
    var fileList = [URL]();
    var colorImageList = [UIImage]();
    var depthImageList = [UIImage]();
    var depthDataList = [[Float]]();
    var pointCloudList = [PointCloud]();
    var testString = "a"
    var CameraIn = [Float](repeating:0.0,count:9);
    var activity = UIActivityIndicatorView()
    let alertController = UIAlertController(title: "Processing..", message: "This may take some time..", preferredStyle: UIAlertControllerStyle.alert)
    let progressBar = UIProgressView(progressViewStyle: .default)
    var continuedBtn = false
    var continueCount = 0
    let systemSoundID: SystemSoundID = 1016
    var transforms = [UnsafeMutablePointer<Double>]();
    let configuration = ARFaceTrackingConfiguration()
    var YuCount = 0;
    
    override func observeValue(forKeyPath keyPath: String?,
                      of object: Any?,
                      change: [NSKeyValueChangeKey : Any]?,
                      context: UnsafeMutableRawPointer?){
        if keyPath == "volumeChanged"{
            if !btnTouched {
                AudioServicesPlaySystemSoundWithCompletion(SystemSoundID(1108), nil)
                btnTouched = true;
            } else {
                //handle logic for user volume button press
            }
        }
    }
    
    func listenVolumeButton(){
        // Option #1
        NotificationCenter.default.addObserver(self, selector: "volumeChanged:", name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
        // Option #2
        do{
            var audioSession = try AVAudioSession()
            try audioSession.setActive(true)
            audioSession.addObserver(self, forKeyPath: "volumeChanged", options: NSKeyValueObservingOptions.new, context: nil)
            
        }catch{
            
        }

    }
    
    @IBOutlet var sceneView: ARSCNView!
    
    @IBOutlet var topTitle: UINavigationItem!
    
    
    @IBAction func Btn(_ sender: UIButton) {
        btnTouched = true
    }
    @IBAction func Cal(_ sender: UIBarButtonItem) {
        let AndyYu = UserDefaults.standard.bool(forKey: "swAndyYu");
        if imgCount < 2 {
            let alertController = UIAlertController(title: "Fail", message: "Not enough pictures.", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        if AndyYu {
            let alertController = UIAlertController(title: "Andy Yu Mode is Enabled", message: "Please disable that function.", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        DispatchQueue.main.async {
            self.alertController.title = "Processing.."
            self.alertController.message = "Please Wait."
            self.progressBar.setProgress(0.0, animated: true)
            self.progressBar.frame = CGRect(x: 10, y: 120, width: 250, height: 0)
            //self.alertController.view.addSubview(self.progressBar)
            
            self.present(self.alertController, animated: true, completion: nil)
            //            self.activity.center = self.view.center
            //            self.activity.hidesWhenStopped = true
            //            self.activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            //            self.view.addSubview(self.activity)
            //            self.activity.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
        DispatchQueue.global().async {
            self.calculate()
        }
    }

    @IBAction func `continue`(_ sender: UIButton) {
        continuedBtn = true;
    }
    
    @IBAction func YuMode(_ sender: UIButton) {
        AudioServicesPlaySystemSoundWithCompletion(SystemSoundID(1108), nil)
        btnTouched = true
    }
    @IBAction func Clear(_ sender: UIBarButtonItem) {
        fileList = [URL]();
        depthImageList = [UIImage]();
        colorImageList = [UIImage]();
        depthDataList = [[Float]]();
        pointCloudList = [PointCloud]();
        imgCount = 0;
        YuCount = 0;
        topTitle.title = "AR View"
        sceneView.session.pause()
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
    
    func calculate2(){
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        let imgMin = UserDefaults.standard.float(forKey: "imgMinDist"); let imgMax = UserDefaults.standard.float(forKey: "imgMaxDist");
        let AndyYu = UserDefaults.standard.bool(forKey: "swAndyYu");let combine = UserDefaults.standard.bool(forKey: "swCombine");
        CameraIn[0] = 500.872600349; CameraIn[4] = CameraIn[0]; CameraIn[6] = 180;CameraIn[7] = 320;
        
        if AndyYu{
            for i in 0..<imgCount{
                DispatchQueue.main.async {
                    self.alertController.title = "Andy Yu Mode Detected.."
                    self.alertController.message = "Generating Pictures. \(i) of \(self.imgCount - 1)"
                    self.progressBar.setProgress(Float(i/self.imgCount-2), animated: true)
                }
            }
        }
        
        var tmpCloud = pointCloudList[0]
        if !AndyYu {
            for j in 1 ..< imgCount {
                for t in 0 ..< j {
                    let idx = j - t - 1
                    pointCloudList[j].transform(transforMAT: transforms[idx])
                }
                if combine && !AndyYu{
                    tmpCloud = tmpCloud + pointCloudList[j]
                }
        
        
                DispatchQueue.main.async {
                    self.alertController.title = "Processing.."
                    self.alertController.message = "Transforming and combining point Cloud(s). \(j) of \(self.imgCount-1)"
                }
            }
        }
        
        if !combine && !AndyYu{
            for i in 0..<imgCount{
                DispatchQueue.main.async {
                    self.alertController.title = "Writing.."
                    self.alertController.message = "Writing data.. \(i) of \(self.imgCount-1)"
                }
                let name = formatter.string(from: date)+"-\(hour)-\(minutes)-\(i).ply"
                fileList.append(pointCloudList[i].writePlyToDoc(filename: name, min: imgMin, max: imgMax)!)
            }
        }else if !AndyYu{
            DispatchQueue.main.async {
                self.alertController.title = "Writing.."
                self.alertController.message = "Writing data.."
            }
            fileList.append(tmpCloud.writePlyToDoc(filename: formatter.string(from: date)+"-\(hour)-\(minutes)-combined.ply", min: imgMin, max: imgMax)!)
        }
        
        DispatchQueue.main.async {
            self.alertController.dismiss(animated: true, completion: nil)
            UIApplication.shared.endIgnoringInteractionEvents()
            self.sceneView.session.pause()
            self.sceneView.session.run(self.configuration, options: [.resetTracking, .removeExistingAnchors])
        }
        let activityController = UIActivityViewController(activityItems: self.fileList, applicationActivities: nil)
        self.present(activityController, animated: true, completion: nil)
    }
    func calculate(){
        fileList = [URL]();
        transforms = [UnsafeMutablePointer<Double>]();
        
        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        let depthSize = CGSize(width: 360, height: 640);
        //        UIImageWriteToSavedPhotosAlbum(colorImageList[1].resizeImage(size: depthSize), nil, nil, nil);
        //        UIImageWriteToSavedPhotosAlbum(depthImageList[1], nil, nil, nil);
        
        
        let siftMin = UserDefaults.standard.float(forKey: "siftMinDist"); let siftMax = UserDefaults.standard.float(forKey: "siftMaxDist");
        let iter = UserDefaults.standard.integer(forKey: "ransacRepeating");let scaler = UserDefaults.standard.integer(forKey: "siftScale");
        let error = UserDefaults.standard.float(forKey: "ransacError");
        let AndyYu = UserDefaults.standard.bool(forKey: "swAndyYu");
        CameraIn[0] = 500.872600349; CameraIn[4] = CameraIn[0]; CameraIn[6] = 180;CameraIn[7] = 320;

        for i in 0..<imgCount-1{
            if !AndyYu {
                let tmp1 = colorImageList[i].resizeImage(size: depthSize)
                let tmp2 = colorImageList[i+1].resizeImage(size: depthSize)
                let tmp = OpenCVWrapper.computeTransForm(tmp1, tmp2, depthDataList[i], 360, CameraIn,siftMin,siftMax,Int32(iter),error,Int32(scaler))!
            
               transforms.append(tmp)
            }
            
            DispatchQueue.main.async {
                self.alertController.title = "Processing.."
                self.alertController.message = "Calculating transform between pictures. \(i+1) of \(self.imgCount-1)"
                self.progressBar.setProgress(Float(i/self.imgCount-2), animated: true)
            }
        }
        
        if AndyYu {
            let imgMin = UserDefaults.standard.float(forKey: "imgMinDist"); let imgMax = UserDefaults.standard.float(forKey: "imgMaxDist");
            let tmp1 = colorImageList[0].resizeImage(size: depthSize)
            let tmp2 = colorImageList[1].resizeImage(size: depthSize)
            let tmp = OpenCVWrapper.computeTransForm(tmp1, tmp2, depthDataList[0], 360, CameraIn,siftMin,siftMax,Int32(iter),error,Int32(scaler))!
            let Name = UserDefaults.standard.string(forKey: "fileName")!
            DispatchQueue.main.async {
                self.alertController.title = "Writing.."
                self.alertController.message = "Writing Point Cloud ply data."
            }
            pointCloudList[1].writePlyToDoc(filename: Name + "-Pt-\(YuCount).ply", min: imgMin, max: imgMax)
            DispatchQueue.main.async {
                self.alertController.message = "Writing Transform data."
                
            }
            writeTxtFile(fileName:Name + "-T-\(YuCount)-to-\(YuCount - 1).csv", textToWrite: "\(tmp[0]),\(tmp[1]),\(tmp[2]),\(tmp[3])\n\(tmp[4]),\(tmp[5]),\(tmp[6]),\(tmp[7])\n\(tmp[8]),\(tmp[9]),\(tmp[10]),\(tmp[11])\n\(tmp[12]),\(tmp[13]),\(tmp[14]),\(tmp[15])\n")!
            DispatchQueue.main.async {
                self.alertController.message = "Writing Image."
                
            }
            saveImageToDocumentDirectory(colorImageList[1], name:Name + "-C-\(YuCount)")
            saveImageToDocumentDirectory(depthImageList[1], name:Name + "-D-\(YuCount)")
            DispatchQueue.main.async {
                self.alertController.message = "Writing depth data."
                
            }
            writeDepthCSV(depthData: depthDataList[1], width: 180, height: 320, filename:Name + "-D-\(YuCount).csv")
        }
        DispatchQueue.main.async {
            self.alertController.dismiss(animated: true, completion: nil)
            UIApplication.shared.endIgnoringInteractionEvents()
        }
        
    }
    
    @IBAction func Share(_ sender: Any) {
        if UserDefaults.standard.bool(forKey: "swAndyYu") {
            let activityController = UIActivityViewController(activityItems: self.fileList, applicationActivities: nil)
            self.present(activityController, animated: true, completion: nil)
            return
        }
        
        if transforms.count <= 0  {
            let alertController = UIAlertController(title: "Fail", message: "Not enough Transfrmation.\nPlease calculate first.", preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
            return
        }
        
        DispatchQueue.main.async {
            //self.alertController.view.addSubview(self.progressBar)
            self.alertController.title = "Processing.."
            self.alertController.message = "Please Wait."
            self.present(self.alertController, animated: true, completion: nil)
            //            self.activity.center = self.view.center
            //            self.activity.hidesWhenStopped = true
            //            self.activity.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
            //            self.view.addSubview(self.activity)
            //            self.activity.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()
        }
        DispatchQueue.global().async {
            self.calculate2()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        sceneView.session.delegate = self
        
        // Show statistics such as fps and timing information
        sceneView.showsStatistics = true
        self.hideKeyboardWhenTappedAround()
        // Create a new scene
        // let scene = SCNScene(named: "art.scnassets/ship.scn")!
        
        // Set the scene to the view
        // sceneView.scene = scene
//        imgMinDist = Float(txfImgMinDist.text!)!
//        imgMaxDist = Float(txfImgMinDist.text!)!
//        siftMinDist = Float(txfSiftMinDist.text!)!
//        siftMaxDist = Float(txfSiftMaxDist.text!)!
//        siftScale = Float(txfSiftScale.text!)!
//        ransacRepeating = Int(txfRansacRepeating.text!)!
//        ransacError = Float(txfRansacError.text!)!
//        ransacInliers = Int(txfRansacInliers.text!)!

        if UserDefaults.standard.string(forKey: "firstRun") == nil {
            UserDefaults.standard.set("set", forKey: "firstRun")
            UserDefaults.standard.set(0.1,forKey:"imgMinDist")
            UserDefaults.standard.set(0.5,forKey:"imgMaxDist")
            UserDefaults.standard.set(0.1,forKey:"siftMinDist")
            UserDefaults.standard.set(0.5,forKey:"siftMaxDist")
            UserDefaults.standard.set(4.0, forKey: "siftScale")
            UserDefaults.standard.set(500,forKey: "ransacRepeating")
            UserDefaults.standard.set(0.5,forKey: "ransacError")
            UserDefaults.standard.set(30,forKey: "ransacInliers")
            UserDefaults.standard.set(false, forKey: "swCombine")
            UserDefaults.standard.set(false, forKey: "swAndyYu")
            UserDefaults.standard.set("data", forKey: "fileName")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // 用于检测是否是 iPhone X
        guard ARFaceTrackingConfiguration.isSupported else {
            print("not iPhone X.")
            return
        }
        print("iPhone X Detected.")
        
        super.viewWillAppear(animated)
        listenVolumeButton()
        
        // Create a session configuration
        //let configuration = ARWorldTrackingConfiguration()
        
        configuration.isLightEstimationEnabled = true


        // Run the view's session
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        sceneView.session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
       
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/

    
    
    func toPointCloud(u:Float,v:Float,pixelSize:Float,Focal:Float,w:Int,h:Int,depth:Float)->(Float,Float,Float){
        let x:Float = depth * (u - Float(360/2)) / 500.872600349;
        let y:Float = depth * (v - Float(640/2)) / 500.872600349;
        let z:Float = depth;
        return (x,y,z)
    }
   
    
    
    // sceneView.session.delegate = self 这句话注册了 ARSession 类在 ViewController 里的委托。
    // 其中的一个委托方法 optional func session(_ session: ARSession, didUpdate frame: ARFrame) 中
    // 每 1/60 秒摄像机画面更新都会调用该部分，其中每4个 frame 里只有一个带有深度信息，也就是说采样频率是15Hz。
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        if frame.capturedDepthData != nil && !captured{
            for i in 0..<3 {
                for j in 0..<3{
                    let count = i * 3 + j
                    CameraIn[count] = frame.camera.intrinsics[i][j]
                }
            }
            captured = true
            print(CameraIn)
        }
        if continuedBtn && continueCount < 120 {
            continueCount += 1;
            if (continueCount == 60 || continueCount == 119){
                DispatchQueue.main.async{
                    AudioServicesPlaySystemSound (self.systemSoundID)
                }
            }
        }


        // 判断 capturedDepthData 是否为空。 nil 可以理解为 NULL 。
        if (frame.capturedDepthData != nil && (btnTouched || (continuedBtn && continueCount >= 120) ) ){
            if continuedBtn{
                if continueCount <= 124{
                    continueCount += 1;
                }else{
                    continueCount = 120;
                }
                
            }
            btnTouched = false
            print("btnTouched")
    
            // frame.capturedDepthData 是 Optional 类型，对 Optional 类型需要使用 ! 对其解包
            // frame.capturedDepthData! 解包操作
            // let 关键字声明的是常量， var 关键字是变量
            // var depthDataMap = frame.capturedDepthData!.depthDataMap
            let depthDataMap = frame.capturedDepthData!.depthDataMap
            let colorDataMap = frame.capturedImage
            print(CVPixelBufferGetPlaneCount(colorDataMap))
            // cw ch RGB 图片的长与宽
            let cw = CVPixelBufferGetWidth(colorDataMap)
            let ch = CVPixelBufferGetHeight(colorDataMap)
            // w h 图片的长与宽
            let w = CVPixelBufferGetWidth(depthDataMap)
            let h = CVPixelBufferGetHeight(depthDataMap)

            // CVPixelBufferLockBaseAddress 用于初始化 CVPixelBuffer 类中数据的指针地址
            CVPixelBufferLockBaseAddress(depthDataMap, CVPixelBufferLockFlags(rawValue: 0))
            
            // unsafeBitCast 是 Swift 中类似于 C++ 的指针强制转换。 类似于 int * a= (double*)b.
            // CVPixelBufferGetBaseAddress 就是获得指针位置的操作。 在 Swift 里直接使用 Pointer 被定义为 Unsafe 操作。
            // 下面这句话转换成 C++ 便是
            // const float * depthPointer = (float*)CVPixelBufferGetBaseAddress(depthDataMap)
            let depthPointer = unsafeBitCast(CVPixelBufferGetBaseAddress(depthDataMap), to: UnsafeMutablePointer<Float32>.self)
            
            // Swift 中 UnsafeMutablePointer<Float32> 类型和 C++ float* 一样也可以使用 [] 当作数组使用。
            // 这样就可以获得像素中的深度信息，depthPointer范围是 [0,w*h-1]，值域以米为单位。应该是从左往右从上往下排列的。
            // 实际测量下来 0.2 - 2.0 都是有可能的。

            //DepthMap to PointCloud
            //.rotated(by: Measurement(value: 48.0, unit: .degrees), options: [.flipOnVerticalAxis])
            var colorData = colorDataMap.image.rotated(by: Measurement(value: 90.0, unit: .degrees),options: [.flipOnHorizontalAxis])!.colorData
            var depthData = Array<Float>(repeating: 0.0,count:w*h);
            var tmpPointCloudData = [[Float]]();
            var tmpColorData = [Pixel]();

            for v in 0..<w{ // height
                for u in 0..<h{ // width
                    let count = v * h + (u);
                    let frCount = u * w + (v);
                    let colorCount = 4*(2*v * ch + (2*u));
                    
                    if !depthPointer[frCount].isNaN{
                        depthData[count] = depthPointer[frCount]
                        let output = toPointCloud(u: Float(u), v: Float(v), pixelSize: 0.000001, Focal: 0.00287, w: w, h: h, depth: depthPointer[frCount]);
                        
                        tmpPointCloudData.append([output.0,output.1,output.2,1.0])
                        tmpColorData.append(Pixel(R: colorData[colorCount], G: colorData[colorCount+1], B: colorData[colorCount+2]))
                        
                    }
                }
            }
            
//            let date = Date()
//            let calendar = Calendar.current
//            let hour = calendar.component(.hour, from: date)
//            let minutes = calendar.component(.minute, from: date)
//
//            let formatter = DateFormatter()
//            formatter.dateFormat = "dd.MM.yyyy"
//            let name = formatter.string(from: date)+"-\(hour)-\(minutes)-\(imgCount).ply"

            
            colorImageList.append(colorDataMap.image.rotated(by: Measurement(value:90.0, unit:.degrees),options: [.flipOnHorizontalAxis])!)
            depthImageList.append(depthDataMap.image.rotated(by: Measurement(value:90.0, unit:.degrees),options: [.flipOnHorizontalAxis])!)
            depthDataList.append(depthData)

            //UIImageWriteToSavedPhotosAlbum(colorImageList[0], nil, nil, nil)
            pointCloudList.append(PointCloud(cloudData: tmpPointCloudData, colorData: tmpColorData))
            // fileList.append(pointCloudList[0].writePlyToDoc(filename: "test.ply")!)
            imgCount += 1;
//            pointCloudList[0].writePlyToDoc(filename: "test.ply", min: 0.1, max: 0.5)
            if UserDefaults.standard.bool(forKey: "swAndyYu"){
                if YuCount >= 2{
                    colorImageList.remove(at: 0)
                    depthImageList.remove(at: 0)
                    depthDataList.remove(at: 0)
                    pointCloudList.remove(at: 0)
                }
                if YuCount >= 1{
                    imgCount = 2;
                    DispatchQueue.main.async {
                        self.alertController.title = "Processing.."
                        self.alertController.message = "Please Wait."
                        self.present(self.alertController, animated: true, completion: nil)
                        UIApplication.shared.beginIgnoringInteractionEvents()
                    }
                    DispatchQueue.global().async {
                        self.calculate()
                    }
                    
                }else if YuCount == 0{
                    DispatchQueue.main.async {
                        self.alertController.title = "Processing.."
                        self.alertController.message = "Please Wait."
                        self.present(self.alertController, animated: true, completion: nil)
                        UIApplication.shared.beginIgnoringInteractionEvents()
                    }
                    DispatchQueue.global().async {
                        let Name = UserDefaults.standard.string(forKey: "fileName")!
                        let imgMin = UserDefaults.standard.float(forKey: "imgMinDist"); let imgMax = UserDefaults.standard.float(forKey: "imgMaxDist");
                        self.pointCloudList[0].writePlyToDoc(filename: Name + "-Pt-\(1).ply", min: imgMin, max: imgMax)
                        saveImageToDocumentDirectory(self.colorImageList[0], name:Name + "-C-\(1)")
                        saveImageToDocumentDirectory(self.depthImageList[0], name:Name + "-D-\(1)")
                        writeDepthCSV(depthData: self.depthDataList[0], width: 180, height: 320, filename:Name + "-D-\(1).csv")
                        DispatchQueue.main.async {
                            self.alertController.dismiss(animated: true, completion: nil)
                            UIApplication.shared.endIgnoringInteractionEvents()
                        }
                    }
                }
                 YuCount += 1;
            }
            
            if UserDefaults.standard.bool(forKey: "swAndyYu"){
                topTitle.title = "\(YuCount) image(s)"
            }else{
                topTitle.title = "\(imgCount) image(s)"
            }
            
            if continuedBtn{
                if imgCount >= UserDefaults.standard.integer(forKey: "ransacInliers"){
                    DispatchQueue.main.async{
                        AudioServicesPlaySystemSound (self.systemSoundID)
                    }
                    continuedBtn = false;
                    continueCount = 0;
                }
            }
            
            
            
        }
        
        
        
        
    }
    

    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required

    }
}
