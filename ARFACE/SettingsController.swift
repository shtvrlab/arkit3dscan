//
//  SettingsController.swift
//  ARFACE
//
//  Created by Tom on 2017. 12. 25..
//  Copyright © 2017년 vrlab. All rights reserved.
//

import UIKit


extension UITableViewController {
    func hideKeyboardWhenTappedAround2() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UITableViewController.dismissKeyboard2))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard2() {
        view.endEditing(true)
    }
}

class SettingsController: UITableViewController{
    @IBOutlet var txfImgMinDist: UITextField!
    @IBOutlet var txfImgMaxDist: UITextField!
    @IBOutlet var txfSiftMinDist: UITextField!
    @IBOutlet var txfSiftMaxDist: UITextField!
    @IBOutlet var txfSiftScale: UITextField!
    @IBOutlet var txfRansacRepeating: UITextField!
    @IBOutlet var txfRansacError: UITextField!
    @IBOutlet var txfRansacInliers: UITextField!
    @IBOutlet var swCombine: UISwitch!
    @IBOutlet var swAndyYu: UISwitch!
    @IBOutlet var txfFileName: UITextField!
    
    func updateTxf(){
        txfImgMinDist.text = String(UserDefaults.standard.float(forKey: "imgMinDist"))
        txfImgMaxDist.text = String(UserDefaults.standard.float(forKey: "imgMaxDist"))
        txfSiftMinDist.text = String(UserDefaults.standard.float(forKey: "siftMinDist"))
        txfSiftMaxDist.text = String(UserDefaults.standard.float(forKey: "siftMaxDist"))
        txfSiftScale.text = String(UserDefaults.standard.float(forKey: "siftScale"))
        txfRansacRepeating.text = String(UserDefaults.standard.integer(forKey: "ransacRepeating"))
        txfRansacError.text = String(UserDefaults.standard.float(forKey: "ransacError"))
        txfRansacInliers.text = String(UserDefaults.standard.integer(forKey: "ransacInliers"))
        txfFileName.text = UserDefaults.standard.string(forKey: "fileName")
        swCombine.isOn = UserDefaults.standard.bool(forKey: "swCombine")
        swAndyYu.isOn = UserDefaults.standard.bool(forKey: "swAndyYu")
    }
    func updateVal(){
        UserDefaults.standard.set(Float(txfImgMinDist.text!)!,forKey:"imgMinDist")
        UserDefaults.standard.set(Float(txfImgMaxDist.text!)!,forKey:"imgMaxDist")
        UserDefaults.standard.set(Float(txfSiftMinDist.text!)!,forKey:"siftMinDist")
        UserDefaults.standard.set(Float(txfSiftMaxDist.text!)!,forKey:"siftMaxDist")
        UserDefaults.standard.set(Float(txfSiftScale.text!)!, forKey: "siftScale")
        UserDefaults.standard.set(Int(txfRansacRepeating.text!)!,forKey: "ransacRepeating")
        UserDefaults.standard.set(Float(txfRansacError.text!)!,forKey: "ransacError")
        UserDefaults.standard.set(Int(txfRansacInliers.text!)!,forKey: "ransacInliers")
        UserDefaults.standard.set(swCombine.isOn, forKey: "swCombine")
        UserDefaults.standard.set(swAndyYu.isOn, forKey: "swAndyYu")
        UserDefaults.standard.set(txfFileName.text!, forKey: "fileName")
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround2()
        updateTxf()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        updateVal()
        super.viewWillDisappear(true)
    }
    // MARK: - Table view data source

//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return 0
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//        return 0
//    }

    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath){
        let section = indexPath.section
        let row = indexPath.row
        
        switch section{
        case 1:
            switch row{
//                let controller = storyboard?.instantiateViewController(withIdentifier: "ARView") as! ViewController
//                print("STRING "+controller.testString);
//                let activityController = UIActivityViewController(activityItems: controller.fileList, applicationActivities: nil)
//                present(activityController, animated: true, completion: nil)
            default:
                break
            }
        default:
            break
        }
        
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
