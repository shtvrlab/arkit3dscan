//
//  OpenCVWrapper.h
//  ARFACE
//
//  Created by vrlab on 2017/12/23.
//  Copyright © 2017年 vrlab. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIkit.h>

@interface OpenCVWrapper : NSObject
- (UIImage *) isThisWorking:(UIImage *) input;
//+(UIImage *)flipnrot:(UIImage *)image;
//+(UIImage *)flipsav:(UIImage *)image;
+(double *) computeTransForm: (const UIImage*) IMG1 : (const UIImage*) IMG2 : (const float[])depthData :(int) w : (const float [])cameraIn:(float) low:(float) high:(int)iterCount:(float)error:(int)inliers;
+(void) testing:(const float [])array;
@end
